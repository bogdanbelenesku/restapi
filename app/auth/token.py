import time
import jwt
from typing import Dict, Union

from app.settings import settings
from app.log.config import logger

from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials


def sign_token(data: Dict) -> Dict:
    payload = {
        "expires": time.time() + settings.token_live
    } | data
    hashed_token = jwt.encode(payload, settings.secret, algorithm=settings.algorithm)

    return hashed_token


def decode_token(token: str) -> Union[Dict, None]:
    try:
        decoded_token: Dict = jwt.decode(token, settings.secret, algorithms=[settings.algorithm])
        return decoded_token if decoded_token["expires"] >= time.time() else None
    except Exception as error:
        logger.warning(error)
        return None


class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            if not (data := self.verify_jwt(credentials.credentials)):
                raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            return data
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    @staticmethod
    def verify_jwt(jwt_token: str) -> Dict:
        payload: Dict = decode_token(jwt_token)

        if payload is not None:
            return payload
        return dict()
