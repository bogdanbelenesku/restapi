import logging
from logging.config import dictConfig

from app.settings import LOG_CONFIG


dictConfig(LOG_CONFIG)
logger = logging.getLogger('server-logger')
