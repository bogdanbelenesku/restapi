import asyncio

from datetime import datetime

from sqlalchemy import select, Sequence
from sqlalchemy import String, Integer, DateTime, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.db import Base, AsyncSession, engine
from passlib.handlers.sha2_crypt import sha512_crypt


class Table(Base):
    __abstract__ = True
    id: Mapped[int] = mapped_column(Integer, autoincrement=True, primary_key=True, index=True)

    @classmethod
    async def objects(cls, session: AsyncSession) -> Sequence:
        result = await session.execute(select(cls))
        return result.scalars().all()

    @classmethod
    async def get_by_property(cls, session: AsyncSession, **kwargs) -> 'Table':
        if kwargs and all([hasattr(cls, name) for name in kwargs.keys()]):
            res = await session.execute(select(cls).where(
                *[getattr(cls, name) == kwargs[name]
                  for name in kwargs.keys()]
            ))
            if output := res.fetchone():
                return output[0]

    async def save(self, session: AsyncSession):
        session.add(self)
        await session.commit()


class Worker(Table):
    __tablename__ = "worker"

    login: Mapped[str] = mapped_column(String(50), unique=True)
    password: Mapped[str] = mapped_column(String(256))
    worker_info: Mapped['WorkerInfo'] = relationship("WorkerInfo", cascade="all, delete-orphan",
                                                     back_populates="worker")

    def __setattr__(self, key, value):
        if key == "password":
            value = sha512_crypt.hash(value)
        super(Worker, self).__setattr__(key, value)

    def check_password(self, value: str) -> bool:
        return sha512_crypt.verify(value, self.password)


class WorkerInfo(Table):
    __tablename__ = "worker_info"

    salary: Mapped[int] = mapped_column(Integer())
    promotion_date: Mapped[datetime] = mapped_column(DateTime())
    worker_id: Mapped[int] = mapped_column(ForeignKey("worker.id"))
    worker: Mapped['Worker'] = relationship("Worker", back_populates="worker_info")

    @property
    def promotion_date_to_str(self) -> str:
        return self.promotion_date.date().isoformat()


async def init_models():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


if __name__ == '__main__':
    asyncio.run(init_models())
