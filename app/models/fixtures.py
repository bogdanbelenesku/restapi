from sqlalchemy import text

from app.models.db import TestingSession
from app.models.tables import WorkerInfo, Worker
from datetime import datetime, timedelta




infos = [
    {
        "login": "lkYre67V",
        "password": "qwerty123",
        "worker_info": {
            "salary": 40_000,
            "promotion_date": datetime.now()
        }
    },
    {
        "login": "Tfs456Mnx_",
        "password": "vasil1984",
        "worker_info": {
            "salary": 80_000,
            "promotion_date": datetime.now() - timedelta(12)
        }
    },
    {
        "login": "enTy20$zz",
        "password": "sweden_beer_2023",
        "worker_info": {
            "salary": 60_000,
            "promotion_date": datetime.now() + timedelta(23)
        }
    }
]

workers = [
    Worker(
        login=data["login"],
        password=data["password"],
        worker_info=WorkerInfo(**data["worker_info"])
    )
    for data in infos
]


def fill_db(session = TestingSession()):
    output = session.execute(text(f"SELECT COUNT(*) FROM {Worker.__tablename__};"))
    if value := output.fetchone():
        if value[0] == 0:
            session.add_all(workers)
            session.commit()
    session.close()


if __name__ == '__main__':
    fill_db()
