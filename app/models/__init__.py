from pydantic import BaseModel, Field

from datetime import datetime


class WorkerInfoSchema(BaseModel):
    salary: int = Field(gt=0)
    promotion_date: str = Field(default=datetime.now().date().isoformat())


class WorkerLoginSchema(BaseModel):
    login: str = Field(min_length=8)
    password: str = Field(min_length=8)
