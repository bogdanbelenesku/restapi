import os
from typing import Dict

from pydantic import BaseSettings
from pydantic.fields import Field


class Settings(BaseSettings):

    response: Dict = {
        "data": [],
        "messages": [],
        "status_code": 200
    }

    login: str
    password: str
    host: str = Field(default="localhost")
    name: str
    port: int = Field(default="5432")

    secret: str = Field(env="secret")
    algorithm: str = Field(env="algorithm")
    token_live: int = Field(env="token_live")

    class Config:
        allow_mutation = False
        root: str = os.path.join(os.path.abspath("."))
        env_file = os.path.join(root, '.env')
        env_prefix = "db_"

    def db_url(self, sync: bool = False) -> str:
        params = ["login", "password", "host", "port", "name"]
        values = [getattr(self, name) for name in params]
        sync = "+asyncpg" if not sync else ""

        return "postgresql{}://{}:{}@{}:{}/{}".format(sync, *values)


LOG_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(asctime)s %(message)s %(filename)20s:%(lineno)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",

        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    },
    "loggers": {
        "server-logger": {"handlers": ["default"], "level": "DEBUG"},
    },
}


settings = Settings()
