import asyncio
import functools
import json

from copy import deepcopy
from typing import List, Dict, Union, Callable, Tuple

from fastapi import FastAPI, Depends, Body, Response
from starlette.routing import Route

from app.models.fixtures import fill_db

from app.models import WorkerLoginSchema, WorkerInfoSchema
from app.models.db import get_session, AsyncSession, sync_engine
from app.models.tables import Worker, WorkerInfo, Base

from app.settings import settings
from app.auth.token import sign_token, JWTBearer


app = FastAPI()


def create_data_format(data_format: Dict) -> Callable:
    def handle_function(function: Callable) -> Callable:
        @functools.wraps(function)
        async def wrapper(*args, **kwargs) -> Tuple[str, int]:
            if asyncio.iscoroutinefunction(function):
                data = await function(*args, deepcopy(data_format), **kwargs)
            else:
                data = function(*args, deepcopy(data_format), **kwargs)
            return json.dumps(data), data.get("status_code", 0)
        return wrapper
    return handle_function


@create_data_format(data_format=settings.response)
def create_api_map(routes: List[Route], data_dict: Dict) -> Dict:
    for route in routes:
        data_dict["data"].append(
            {
                "url": route.path,
                "methods": list(route.methods)
            }
        )
    data_dict["messages"].append("API routing map")
    return data_dict


@create_data_format(data_format=settings.response)
async def auth_worker(session: AsyncSession, obj: WorkerLoginSchema, data_dict: Dict) -> Dict:
    worker = await Worker.get_by_property(session, **{"login": obj.login})
    if worker:
        if worker.check_password(obj.password):
            auth_token = sign_token({"worker_id": worker.id, "login": worker.login})
            data_dict["data"] = {"token": auth_token}
            data_dict["status_code"] = 202
        else:
            data_dict["messages"].append("Password is incorrect")
            data_dict["status_code"] = 401
    else:
        data_dict["messages"].append("Worker not found")
        data_dict["status_code"] = 404
    return data_dict


@create_data_format(data_format=settings.response)
async def send_worker_info(session: AsyncSession, worker_id: Union[int, None], data_dict: Dict) -> Dict:
    if worker_id is not None:
        obj: WorkerInfo = await WorkerInfo.get_by_property(session, **{"worker_id": worker_id})
        if obj:
            worker_info: WorkerInfoSchema = WorkerInfoSchema(salary=obj.salary, promotion_date=obj.promotion_date_to_str)
            data_dict["data"] = worker_info.dict()
            data_dict["status_code"] = 202
        else:
            data_dict["messages"].append(f"Worker with {worker_id} id not found")
            data_dict["status_code"] = 404
    else:
        data_dict["messages"].append(f"Invalid worker id")
        data_dict["status_code"] = 400
    return data_dict


@app.on_event("startup")
async def app_startup():
    Base.metadata.drop_all(bind=sync_engine)
    Base.metadata.create_all(bind=sync_engine)
    fill_db()


@app.get("/")
async def get_api_map():
    content, code = await create_api_map(app.routes)
    return Response(content, code, media_type='application/json')


@app.post("/get_token")
async def get_token(
        worker: WorkerLoginSchema = Body(...),
        session: AsyncSession = Depends(get_session)
):
    content, code = await auth_worker(session, worker)
    return Response(content, code, media_type='application/json')


@app.post("/get_info")
async def get_worker_info(session: AsyncSession = Depends(get_session),
                          payload: Dict = Depends(JWTBearer())):
    _id: Union[int, None] = payload.get("worker_id", None)
    content, code = await send_worker_info(session, _id)

    return Response(content, code, media_type='application/json')
