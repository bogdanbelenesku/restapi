import json

import pytest

from typing import List, Dict

from app.auth.token import sign_token
from app.models.db import Base, TestingSession, sync_engine
from app.server import app

from app.models.tables import Worker
from app.models.fixtures import workers, infos

from httpx import AsyncClient
from starlette.routing import Route


@pytest.fixture(scope="module")
def session():

    Base.metadata.drop_all(bind=sync_engine)
    Base.metadata.create_all(bind=sync_engine)

    db = TestingSession()
    db.add_all(workers)
    db.commit()

    try:
        yield db
    finally:
        db.close()
        Base.metadata.drop_all(bind=sync_engine)


@pytest.mark.anyio
async def test_api_status():
    routes: List[Route] = [
        route for route in app.routes
        if route.path in ["/", "/get_token", "/get_info"]
    ]
    for route in routes:
        for method in route.methods:
            async with AsyncClient(app=app, base_url="http://localhost") as cl:
                request = getattr(cl, method.lower())
                response = await request(route.path)
                assert not str(response.status_code).startswith("5")


@pytest.mark.anyio
async def test_get_info(session):
    worker: Worker = workers[0]
    expected_data: Dict = {
        "salary": worker.worker_info.salary,
        "promotion_date": worker.worker_info.promotion_date_to_str
    }
    auth_token = f"Bearer {sign_token({'worker_id': worker.id, 'login': worker.login})}"
    headers = {"Authorization": auth_token}

    async with AsyncClient(app=app, base_url="http://localhost") as cl:
        response = await cl.post("/get_info", headers=headers)
    out_data = json.loads(response.content.decode("utf-8"))

    assert out_data["data"] == expected_data


@pytest.mark.anyio
async def test_get_token(session, worker: Dict = infos[0]):
    in_data = json.dumps({"login": worker["login"], "password": worker["password"]})

    async with AsyncClient(app=app, base_url="http://localhost") as cl:
        response = await cl.post("/get_token", data=in_data)
    out_data = json.loads(response.content.decode("utf-8"))

    assert out_data["data"].get("token", None) is not None
